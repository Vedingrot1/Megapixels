#!/bin/sh

# Install dependencies in as dependency of the virtual "megapixels-development" package
# to allow quick uninstalling of all things added by this development env
apk add -t megapixels-development git meson gcc musl-dev libconfig-dev linux-headers tiff-dev feedbackd-dev glib-dev gtk4.0-dev libxrandr-dev zbar-dev pulseaudio-dev

# Get sources
git clone https://gitlab.com/megapixels-org/libmegapixels.git
git clone https://gitlab.com/megapixels-org/libdng.git
git clone https://gitlab.com/megapixels-org/megapixels.git

# Build libmegapixels
cd libmegapixels
meson setup build
cd build
meson compile
meson install
cd ..
cd ..

# Build libdng
cd libdng
meson setup build
cd build
meson compile
meson install
cd ..
cd ..

# Build Megapixels
cd megapixels
meson setup build
cd build
meson compile
meson install
cd ..
cd ..

# Install gschema
glib-compile-schemas /usr/local/share/glib-2.0/schemas/
